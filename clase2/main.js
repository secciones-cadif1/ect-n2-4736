const {app,BrowserWindow,dialog,ipcMain} = require("electron")

process.env["ELECTRON_DISABLE_SECURITY_WARNINGS"] = true

let mainWin;

app.on("ready",()=>{
    mainWin = new BrowserWindow({
        webPreferences:{
            preload: __dirname + "\\index\\preload-index.js"
        }
    })
    mainWin.loadFile(__dirname+"\\index\\index.html")
})

ipcMain.on("mostrar-mensaje",(event,nombre)=>{
    // id del proceso que envia el mensaje
    console.log(event.processId)
    // es el webContent del proceso que envia el mensaje
    //event.sender.toggleDevTools()

    dialog.showMessageBox(null,{
        title: "Prueba de IPC",
        message: "Ha funcionado "+nombre,
        type : "error",
        buttons : ["Esta bien"],
        noLink :true
    })
})

ipcMain.on("registrar",(event,datos)=>{
    console.log(datos)
    dialog.showMessageBox(null,{
        title: "Registro exitoso",
        message: "Se registró exitosamente el cliente "+datos.nombre.toUpperCase()+
                 " con el número de teléfono "+datos.telefono,
        type : "info",
        buttons : ["Esta bien"],
        noLink :true
    })
    setTimeout(()=>{
        valor = Math.random()*10
        console.log(valor)
        event.returnValue = valor 
    },1000)
})

ipcMain.on("abrir-registro",()=>{
    winRegistro = new BrowserWindow({
        show:false,
        webPreferences:{
            preload: __dirname + "\\registro\\preload-registro.js"
        }
    })
    winRegistro.setMenu(null)
    winRegistro.loadFile(__dirname+"\\registro\\registro.html")
    .then(()=>{
        winRegistro.show()
        winRegistro.webContents.openDevTools()
    })
    .catch(()=>{
        dialog.showErrorBox("No se cargo la página")
    })
})

ipcMain.on("cerrar",()=>{
    console.log("cerrando....")
    app.quit()
})

ipcMain.on("imprimir-factura",()=>{
    imprimir()
})

function imprimir(){ 
    mainWin.webContents.print({
        silent:true,
        landscape :true,
        deviceName: "Microsoft Print to PDF"
    },(success,failureReason )=>{
        console.log("luego de intentar imprimir")
        if (!success)
            dialog.showErrorBox("error","No se pudo imprimir. razon: "+failureReason)
        else    
            console.log("se imprimio exitosamente")
    })
}