const {contextBridge,ipcRenderer} = require("electron")

console.log("preload index")

contextBridge.exposeInMainWorld("api",{
    cerrar: ()=>{
        console.log("cerrando en el preload ...")
        ipcRenderer.send("cerrar")
    },
    imprimir: ()=>{
        console.log("imprimiendo en el preload ...")
        ipcRenderer.send("imprimir-factura")
    },
    mostrarMensaje :()=>{
        ipcRenderer.send("mostrar-mensaje","maria")
    },
    registrar : ()=>{
        ipcRenderer.send("abrir-registro")
    }
})
