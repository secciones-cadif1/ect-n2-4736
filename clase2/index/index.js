

window.onload = ()=>{
    document.getElementById("btn-cerrar").onclick = ()=>{
        console.log("cerrando...")
        api.cerrar()
    }

    document.getElementById("btn-imprimir").onclick = ()=>{
        console.log("imprimiendo..")
        api.imprimir()
    }

    document.getElementById("btn-mostrar-mensaje").onclick = ()=>{
        api.mostrarMensaje()
    }

    document.getElementById("btn-registrar").onclick = ()=>{
        api.registrar()
    }
}