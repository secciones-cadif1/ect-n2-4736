const {app,BrowserWindow,Menu, webContents,dialog} = require("electron")

let main;
let puedeImprimir = false;
let ventanaFactura;

process.env["ELECTRON_DISABLE_SECURITY_WARNINGS"] = true

let menu = Menu.buildFromTemplate([
    {
        label: "Imprimir",
        click: ()=>{
            if (puedeImprimir)
                imprimir()
            else
                dialog.showErrorBox("Error","No puede imprimir porque no hay impresoras")
        }
    },
    {
        label :"Factura",
        click:()=>{
            if (!puedeImprimir)
                dialog.showErrorBox("Error","No puede imprimir porque no hay impresoras")
            else{
                winFactura = new BrowserWindow()
                winFactura.setMenu(null)
                winFactura.loadFile("factura.html")
                winFactura.webContents.on("did-finish-load",()=>{
                    console.log("finish load")
                    setTimeout(()=>{
                        winFactura.webContents.print({
                            silent:false
                        },(success,failureReason)=>{
                            winFactura.close()
                        })
                    },100)
                })
            }
        }
    },
    {
        label:"prueba",
        click: ()=>{
            if (!ventanaFactura){
                ventanaFactura = new BrowserWindow({
                    webPreferences:{
                        preload: __dirname + "\\preload-factura.js",
                        contextIsolation :true,
                        nodeIntegration :true
                    }
                })
                ventanaFactura.webContents.openDevTools()
                
                setTimeout(()=>{
                    ventanaFactura.loadFile("factura.html")
                },2000)
                
                ventanaFactura.on("closed",()=>{
                    ventanaFactura = undefined
                })
            }else
                ventana.show()
        }
    }
])

function imprimir(){ 
    main.webContents.print({
        silent:true,
        scaleFactor :1,
        landscape :true,
        deviceName: "Microsoft Print to PDF"
    },(success,failureReason )=>{
        console.log("luego de intentar imprimir")
        if (!success)
            dialog.showErrorBox("error","No se pudo imprimir. razon: "+failureReason)
        else    
            console.log("se imprimio exitosamente")
    })
}

function mostrarImpresoras(){
    // obtengo la informacion de todas las impresoras instaladas en el sistema
    main.webContents.getPrintersAsync()
    .then((impresoras)=>{
        
        if (impresoras.length == 0)
            dialog.showMessageBox({
                title:"Error",
                message:"No hay impresoras instaladas",
                type:"error"
            })
        else{
            puedeImprimir = true
            //console.log(impresoras) 
        }
    })
}

app.on("ready",()=>{
    app.applicationMenu = menu
    main = new BrowserWindow({
        title: "Clase 1 - Nivel 2"
    })
    main.loadURL("https://google.com")
    .then(()=>{
        mostrarImpresoras()
    })
})