const {nativeTheme,Tray,Notification,Menu,shell,app,BrowserWindow,ipcMain} = require("electron")
const path = require("path")

// si la aplicacion esta empaquetada, es decir, si esta corriendo el ejecutable
if (app.isPackaged){
    rutaIconoYouTB = path.join("resources","youtube.ico")
    rutaLogo = path.join("resources","logo.ico")
}else{
    rutaIconoYouTB = path.join(__dirname,"youtube.ico")
    rutaLogo = path.join(__dirname,"logo.ico")
}
app.on("ready",()=>{
    nativeTheme.themeSource = 'dark'
    tray = new Tray(rutaIconoYouTB )
    const contextMenu = Menu.buildFromTemplate([
        { label: 'Item1'  },
        { label: 'Item2' },
        { label: 'Modo oscuro', type: 'checkbox', checked: true ,
            click:(event)=>{
                //console.log(event)
                if (event.checked)
                    nativeTheme.themeSource = 'dark';
                else
                    nativeTheme.themeSource = 'system';
            }},
        { type: "separator" },
        { label: 'Salir',
          click: ()=>{
            app.quit();
          } }
    ])
    tray.setToolTip('YouTube')
    tray.setContextMenu(contextMenu)

    let win = new BrowserWindow({
        webPreferences:{
            nodeIntegration :true,
            contextIsolation :false
        }
    })
    win.loadFile("index.html")

    app.setAppUserModelId = "prueba"

    setTimeout(()=>{
        notificar('Basic Notification',
                  'Notification from the Main process')
    },5000)

})

ipcMain.on("notificar",(event,dato)=>{
    notificar("Aviso","Se terminó el tiempo "+dato)
})

function notificar(titulo,mensaje){
    let n = new Notification({
        title: titulo,
        body: mensaje,
        icon: rutaLogo,
        silent: true,
        //timeoutType: "never"
    })
    n.on("close",()=>{
        console.log("Cerrada")
    })
    n.on("click",()=>{
        console.log("click en la notificacion")
        shell.openExternal('https://github.com')
        shell.beep()
        shell.openPath("c:\\users")
    })
    n.show()
}