const {ipcRenderer,contextBridge} = require("electron")

contextBridge.exposeInMainWorld("api",{
    getProductos: ()=>{
        ipcRenderer.invoke("getProductos")
    },
    guardarProducto:(producto)=>{
        ipcRenderer.invoke("savaProducto",producto)
    }
})

ipcRenderer.on("mostrarProductos",(event,productos)=>{
    console.log(productos)
    mostrarProductos(productos)
})

function mostrarProductos(productos){
    console.log(productos)
    tabla = document.getElementById("tabla-productos")
    debugger
    if (productos.length == 0)
        console.log("no hay productos")
    else{
        while (tabla.rows.length>1)
            tabla.deleteRow(1);

        for (const prod of productos) {
            console.log(prod)
            // se agrega una fila por cada producto
            fila = tabla.insertRow(-1)

            for (atributo in prod) {
                // se agrega una celda por cada campo
                celda = fila.insertCell(-1)
                celda.innerHTML = prod[atributo]
            }
        }
    }
}