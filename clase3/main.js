const {app,BrowserWindow,ipcMain,dialog} = require("electron")
const fs = require("fs")
const path = require("path")

let win1,win2,winProductos;



app.on("ready",()=>{
    winProductos = new BrowserWindow({
        webPreferences:{
            preload: __dirname + "\\preload-productos.js"
        }
    });
    winProductos.loadFile("productos.html")
  /*  win1 = new BrowserWindow({
        x:0,
        y:0,
        width:500,
        title:"Ventana 1",
        webPreferences: {
            preload: __dirname+"\\preload-win-1.js"
        }
    })
    win1.loadFile("win-1.html")
    win2 = new BrowserWindow({
        x:500,
        y:0,
        width:500,
        title: "Ventana 2",
        webPreferences: {
            preload: __dirname+"\\preload-win-1.js"
        }
    })
    win1.loadFile("win-1.html")
    win2.loadFile("win-1.html")*/
})

let productos=[];
// concatenar la ruta con el nombre el archivo usando el separado de rutas
// segun el sistema operativo que se esta usando
nombreArchivo =  path.join(__dirname,"productos.json")


ipcMain.handle("getProductos",(event)=>{ 

    fs.readFile(nombreArchivo,"utf8",(err,data)=>{
        console.log(data)
        try{
            datos = JSON.parse(data)
            console.log(datos)
            
        }catch(error){
            console.log("no se puedo procesar el archivo")
        }
        productos = datos
        event.sender.send("mostrarProductos",datos)
    })
})

ipcMain.handle("savaProducto",(event,producto)=>{
    productos.push(producto)
    fs.writeFile(nombreArchivo,JSON.stringify(productos),(err)=>{
        if (err == null)
            console.log("se creo el archivo")
        else
            console.log("error al crear el archivo:"+err)

        return {
            error:err,
            producto: datos.nombre
        }
    })
})

ipcMain.handle("enviar",(event,dato)=>{
    console.log("llego la respuesta..")
    console.log(dato)
    
    if (event.sender.id == 1)
        win2.webContents.send("respuesta",dato.mensaje)
    else
        win1.webContents.send("respuesta",dato.mensaje)

})



ipcMain.on("consulta",(event)=>{
    // envia al render un mensaje
    console.log("manejador de consulta...")
    setTimeout(()=>{
        event.reply("respuesta")
    },3000)
    
})